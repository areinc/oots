$(document).ready(function() {
	$("input:text").click(function() {
		$(this).parent().find("input:file").click();
	});

	var fileSelected = false;
	$('input:file', '.ui.action.input').on('change', function(e) {
		var name = e.target.files[0].name;
		$('input:text', $(e.target).parent()).val(name);
		fileSelected = true;
		enableUploadButton();
	});
	
	var fileTypeSelected = false;
	$('#documentType').on('change', function() {
		if($(this).val()!="") {
			fileTypeSelected = true;
		}
		enableUploadButton();
	});
	
	function enableUploadButton() {
		if (fileSelected && fileTypeSelected) {
			$('#documentUpload').prop('disabled', false);
		}
	}

	var documentUpload = false;
	$('#documentUpload').click(function() {
		documentUpload = true;
	});
	
	$('form').submit(function(event) {
	    if(documentUpload) {
	    	$('form').attr("enctype","multipart/form-data");
	    	var input = $("<input>").attr("type", "hidden").attr("name", "documentUpload").val("documentUpload");
	    	$('form').append(input);
	    }
	});
});