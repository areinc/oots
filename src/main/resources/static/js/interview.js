$(document).ready(function() {
	
	$('#dobCalendar').calendar({
		type: 'date',
		startMode: 'year'
	});
	
	$('#dobCalendar1').calendar({
		type: 'date',
		startMode: 'year'
	});
	
	$('#appointmentCalendar').calendar();
	
	$('#appointmentCalendar1').calendar();
	
	$('#insuranceExpDate').calendar({
		type: 'month',
		startMode: 'year'
	});
	
	$("#copyAddressDiv").checkbox('setting', 'onChange', function () {
	    var ship = $(this).is(":checked");
	    
	    if (ship) {
		      $("[name='physicalAddress']").val($("[name='postalAddress']").val());
		      $("[name='physicalState']").val($("[name='postalState']").val());
		      $("[name='physicalCountry']").val($("[name='postalCountry']").val());
		      $("[name='physicalZipcode']").val($("[name='postalZipcode']").val());
		}
	});
	
	$("input[name='mainPhone'], " +
		"input[name='mobilePhone'], " +
		"input[name='workPhone'], " +
		"input[name='tutorPhone']," +
		"input[name='ecMainPhone']," +
		"input[name='ecMobilePhone']," +
		"input[name='ecWorkPhone']").keyup(function() {
	    $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d)+$/, "($1) $2-$3"));
	});
	
	$('#scholarity').on('change', function() {
		var selection = $('#scholarity').val();
		console.log(selection);
		
		
		if (selection === "OTHER") {
			$('#otherInputDiv').show();
			$("[name='otherInput']").val("");
		} else {
			$('#otherInputDiv').hide();
			$("[name='otherInput']").val("");
		}
	});
	
	$("input[name='incomeSalary']," +
		"input[name='incomeSocialSecurity'], " +
		"input[name='incomePension']," +
		"input[name='incomeEBT']," +
		"input[name='incomeChildSupport']," +
		"input[name='incomeWellness']," +
		"input[name='incomeRent']," +
		"input[name='incomeUnemployment']," +
		"input[name='incomeOwnBusiness']," +
		"input[name='incomeOther']").bind('keypress', function(e){
			var keyCode = (e.which)?e.which:event.keyCode
			return !(keyCode>31 && (keyCode<48 || keyCode>57)); 
	});
	
	$("input[name='incomeSalary']," +
			"input[name='incomeSocialSecurity'], " +
			"input[name='incomePension']," +
			"input[name='incomeEBT']," +
			"input[name='incomeChildSupport']," +
			"input[name='incomeWellness']," +
			"input[name='incomeRent']," +
			"input[name='incomeUnemployment']," +
			"input[name='incomeOwnBusiness']," +
			"input[name='incomeOther']").keyup(function(event) {
		
		// skip for arrow keys
		if(event.which >= 37 && event.which <= 40) return;
		
		$(this).formatCurrency();
	});
	
	$('#addEducation').click(function(){
		$('#educationModal').modal('show');    
	});
	
	/***
	 * Education section
	 */
	function replaceItems (html) {
		console.log("SUCCESS: ", html);
	    $('#items').replaceWith($(html));
	}

	$('button[name="addScholarity"]').click(function (event) {
	    event.preventDefault();
	    var data = $('form').serialize();
	    data += '&addScholarity';
	    
	    $.ajax({
	        type : "POST",
	        url : "/patient",
	        data: data,
	        dataType: 'json',
	        cache: false,
	        success : function(result) {
	        	var items = $('<div />').append(result).find('#showresults').html();
	        	console.log("SUCCESS: ", items);
	        	replaceItems(items);
	            },
	        error : function(e) {
	                console.log("ERROR: ", e);
	            },
	        done : function(e) {
	                console.log("DONE");
	            }
	        });
	});

	$(document).on('click', '#removeScholarity', function (event) {
	    event.preventDefault();
	    var data = $('form').serialize();
	    data += '&removeScholarity=' + $(this).val();
	    $.post('/patient', data, replaceItems);
	});
});