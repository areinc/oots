package com.oots.web.session;

import java.util.List;

import com.oots.core.persistence.model.User;
import com.oots.core.persistence.model.record.Record;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SessionObj {
	private User user;
	private List<Record> records;
}
