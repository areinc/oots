package com.oots.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.oots.web.controller.mapping.MappingURL;

public class SessionInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		//TODO: Add logic to logout if usersession does not exists
		System.out.println("ENTER INTERCEPTOR FOR URL: " + request.getRequestURL());
		
		HttpSession session = request.getSession(false);
		
		if (session != null && session.getAttribute("sessionObj") == null) {
			response.sendRedirect(MappingURL.LOGOUT);
			return false;
		}
		
		return super.preHandle(request, response, handler);
	}
}
