 package com.oots.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.oots.service.implementation.PatientService;
import com.oots.web.controller.mapping.MappingPath;
import com.oots.web.controller.mapping.MappingURL;
import com.oots.web.controller.mapping.NavigationMenu;
import com.oots.web.dto.PatientSearch;
import com.oots.web.dto.patient.PatientDTO;

@Controller
public class PatientListController {
	@Autowired
	private PatientService patientService;
	
	@ModelAttribute("menu")
    public NavigationMenu addAttributes() {
        return NavigationMenu.PATIENTS;
    }

	@GetMapping(MappingURL.PATIENT_LIST)
    public String oots(Model model) {
		model.addAttribute("search", new PatientSearch());
        return MappingPath.PATIENT_LIST;
    }
	
	@PostMapping(MappingURL.PATIENT_LIST)
	public String post(PatientSearch search, BindingResult errors, Model model) {
		
		List<PatientDTO> patientList = patientService.search(search);
		
		search.toString();
		
		model.addAttribute("patients", patientList);
		model.addAttribute("search", search);
		
        return MappingPath.PATIENT_LIST;
    }
}
