package com.oots.web.controller;

import java.text.ParseException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.oots.service.implementation.PatientService;
import com.oots.service.implementation.RecordService;
import com.oots.service.implementation.UserService;
import com.oots.web.controller.mapping.MappingPath;
import com.oots.web.controller.mapping.MappingURL;
import com.oots.web.controller.mapping.NavigationMenu;
import com.oots.web.dto.record.RecordDTO;

@Controller
public class RecordController {
	private static final String AJAX_HEADER_NAME = "X-Requested-With";
    private static final String AJAX_HEADER_VALUE = "XMLHttpRequest";
	
	@Autowired
	private RecordService recordService;
	
	@Autowired
	UserService	userService;
	
	@Autowired
	PatientService patientService;
	
	@ModelAttribute("menu")
    public NavigationMenu menuAttribute() {
        return NavigationMenu.RECORDS;
    }
	
	@ModelAttribute("users")
    public HashMap<String, String> usersAttribute() {
        return userService.getAllActiveUsersNames();
    }
	
	@ModelAttribute("patients")
    public HashMap<String, String> patientsAttribute() {
        return patientService.getAllPatientsNames();
    }
	
	@GetMapping(MappingURL.RECORD)
    public String get(Model model) {
    	model.addAttribute("record", new RecordDTO());
    	
        return MappingPath.RECORD;
    }
	
	@GetMapping(MappingURL.RECORD + "/{id}")
	public String get(@PathVariable("id") Long id, Model model) {		
		try {
			RecordDTO record = recordService.findByID(id);
			record.getPatient().getName().getFullName();
			model.addAttribute("record", record);
		} catch (NullPointerException | ParseException e) {
			e.printStackTrace();
		}
		
		return MappingPath.RECORD;
	}
	
	@PostMapping(MappingURL.RECORD)
	public String post(RecordDTO record, BindingResult errors, Model model) {
//		try {
//			RecordDTO newRecord = recordService.save(record);
//			model.addAttribute("record", newRecord);
//		} catch (NullPointerException | ParseException e) {
//			e.printStackTrace();
//			model.addAttribute("record", record);
//		}
		
		model.addAttribute("record", record);
		
        return MappingPath.RECORD;
    }
	
	@PostMapping(params = "documentUpload", path = {MappingURL.RECORD})
    public String addDocument(RecordDTO record, @RequestParam("file") MultipartFile file, HttpServletRequest request, Model model) {
		model.addAttribute("record", record);
		
        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
             return MappingPath.RECORD + "::#items";
        } else {
            return MappingPath.RECORD;
        }
    }
}
