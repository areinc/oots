package com.oots.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.oots.core.persistence.model.record.Record;
import com.oots.service.implementation.UserService;
import com.oots.web.controller.mapping.NavigationMenu;
import com.oots.web.session.SessionObj;

@Controller
public class HomeController {
	@Autowired
	private UserService userService;
	
	@ModelAttribute("menu")
    public NavigationMenu addAttributes() {
        return NavigationMenu.HOME;
    }
    
	@GetMapping("/home")
    public String oots(HttpServletRequest request, Model model) {
		final HttpSession session = request.getSession(false);
        
		if (session != null) {
        	SessionObj sessionObj = (SessionObj) session.getAttribute("sessionObj");
        	List<Record> records = userService.getUserRecords(sessionObj.getUser().getId());
        	
        	sessionObj.setRecords(records); 
        	session.setAttribute("sessionObj", sessionObj);
        	
        	model.addAttribute("records", records);
        }
		
        return "home";
    }
}
