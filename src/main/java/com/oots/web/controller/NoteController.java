package com.oots.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.oots.web.controller.mapping.MappingPath;
import com.oots.web.controller.mapping.MappingURL;
import com.oots.web.dto.patient.PatientDTO;

@Controller
public class NoteController {
	
	@GetMapping(MappingURL.NOTE_INITIAL_INTERVIEW)
    public String initialInterview(Model model) {
    	model.addAttribute("interview", new PatientDTO());    	
        return MappingPath.NOTE_INITIAL_INTERVIEW;
    }
	
	@GetMapping(MappingURL.NOTE_PROGRESS)
    public String progressNote(Model model) {
    	model.addAttribute("interview", new PatientDTO());
        return MappingPath.NOTE_PROGRESS;
    }
	
	@GetMapping(MappingURL.NOTE_SERVICE_PLAN)
    public String servicePlan(Model model) {
    	model.addAttribute("interview", new PatientDTO());
        return MappingPath.NOTE_SERVICE_PLAN;
    }
	
	@GetMapping(MappingURL.NOTE_WEIGHING)
    public String weighing(Model model) {
    	model.addAttribute("interview", new PatientDTO());
        return MappingPath.NOTE_WEIGHING;
    }
	
	@GetMapping(MappingURL.NOTE_CLOSING_REPORT)
    public String closingReport(Model model) {
    	model.addAttribute("interview", new PatientDTO());
        return MappingPath.NOTE_CLOSING_REPORT;
    }
	
	@GetMapping(MappingURL.NOTE_TRANSFER)
    public String transfer(Model model) {
    	model.addAttribute("interview", new PatientDTO());
        return MappingPath.NOTE_TRANSFER;
    }
	
	@GetMapping(MappingURL.NOTE_DISCUSSION)
    public String discussion(Model model) {
    	model.addAttribute("interview", new PatientDTO());
        return MappingPath.NOTE_DISCUSSION;
    }
}
