package com.oots.web.controller;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.oots.service.implementation.PatientService;
import com.oots.web.controller.mapping.MappingPath;
import com.oots.web.controller.mapping.MappingURL;
import com.oots.web.controller.mapping.NavigationMenu;
import com.oots.web.dto.patient.EducationDTO;
import com.oots.web.dto.patient.PatientDTO;

@Controller
public class PatientController {
	@Autowired
	private PatientService patientService;
	
	@ModelAttribute("menu")
    public NavigationMenu addAttributes() {
        return NavigationMenu.PATIENTS;
    }
	
	private static final String AJAX_HEADER_NAME = "X-Requested-With";
    private static final String AJAX_HEADER_VALUE = "XMLHttpRequest";

	@GetMapping(MappingURL.PATIENT)
    public String get(Model model) {
		PatientDTO patient = new PatientDTO();

    	model.addAttribute("patient", patient);
        return MappingPath.PATIENT;
    }
	
	@PostMapping(MappingURL.PATIENT)
	public String post(PatientDTO patient, BindingResult errors, Model model) {
		try {
			PatientDTO p = patientService.save(patient);
			model.addAttribute("patient", p);
		} catch (NullPointerException | ParseException e) {
			e.printStackTrace();
		}
		
        return MappingPath.PATIENT;
    }
	
	@PostMapping(MappingURL.PATIENT+"/{id}")
	public String get(@PathVariable("id") Long patientId, Model model) {
		try {
			PatientDTO p = patientService.getPatientDTOByID(patientId);
			model.addAttribute("patient", p);
		} catch (NullPointerException | ParseException e) {
			e.printStackTrace();
		}
		
		return MappingPath.PATIENT;
    }
	
	@PostMapping(params = "addScholarity", path = {MappingURL.PATIENT})
    public String addScholarity(PatientDTO patient, HttpServletRequest request, Model model) {
		patient.getAcademicHistory().getEducation().add(new EducationDTO());
        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
             return MappingPath.PATIENT + "::#items";
        } else {
            return MappingPath.PATIENT;
        }
    }

    @PostMapping(params = "removeScholarity", path = {MappingURL.PATIENT})
    public String removeOrder(PatientDTO patient, @RequestParam("removeScholarity") int index, HttpServletRequest request, Model model) {
    	patient.getAcademicHistory().getEducation().remove(index);
        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
            return MappingPath.PATIENT + "::#items";
        } else {
            return MappingPath.PATIENT;
        }
    }
}
