package com.oots.web.controller.mapping;

public class MappingURL {
	public static final String PATIENT_LIST = "/patients";
	public static final String PATIENT = "/patient";
	public static final String RECORD_LIST = "";
	public static final String RECORD = "/record";
	public static final String NOTE_INITIAL_INTERVIEW = "/initialInterview";
	public static final String NOTE_PROGRESS = "/progressNote";
	public static final String NOTE_SERVICE_PLAN = "/servicePlan";
	public static final String NOTE_WEIGHING = "/weighing";
	public static final String NOTE_CLOSING_REPORT = "/closingReport";
	public static final String NOTE_TRANSFER = "/transfer";
	public static final String NOTE_DISCUSSION = "/discussion";
	public static final String LOGOUT = "/logout";
}