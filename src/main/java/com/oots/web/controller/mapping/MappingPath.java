package com.oots.web.controller.mapping;

public class MappingPath {
	public static final String PATIENT_LIST = "patients";
	public static final String PATIENT = "patient/index";
	public static final String RECORD_LIST = "";
	public static final String RECORD = "record/index";
	public static final String NOTE_INITIAL_INTERVIEW = "note/initialInterview";
	public static final String NOTE_PROGRESS = "note/progressNote";
	public static final String NOTE_SERVICE_PLAN = "note/servicePlan";
	public static final String NOTE_WEIGHING = "note/weighing";
	public static final String NOTE_CLOSING_REPORT = "note/closingReport";
	public static final String NOTE_TRANSFER = "note/transfer";
	public static final String NOTE_DISCUSSION = "note/discussion";
}