package com.oots.web.controller.mapping;

public enum NavigationMenu {
	HOME, PATIENTS, RECORDS, DOCUMENTS;
}
