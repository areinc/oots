package com.oots.web.util;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateConverter {

	public static Date dateFromString(String date) throws NullPointerException, ParseException {
    	if (StringUtils.isEmpty(date)) {
    		throw new NullPointerException();
    	}
    	
    	DateTimeFormatter fmt = DateTimeFormat.forPattern("d 'de' MMMM 'del' yyyy").withLocale(Locale.forLanguageTag("es-PR"));
    	DateTime dob = fmt.parseDateTime(date);
		return dob.toDate();
	}
	
	public static String stringFromDate(Date date) throws NullPointerException, ParseException {
    	if (date == null) {
    		throw new NullPointerException();
    	}
    	
		return new DateTime(date).toString("d 'de' MMMM 'del' yyyy", Locale.forLanguageTag("es-PR"));
	}
}
