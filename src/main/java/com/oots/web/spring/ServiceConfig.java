package com.oots.web.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.oots.web.service" })
public class ServiceConfig {
}
