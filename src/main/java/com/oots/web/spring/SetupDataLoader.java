package com.oots.web.spring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.oots.core.persistence.dao.PatientRepository;
import com.oots.core.persistence.dao.PrivilegeRepository;
import com.oots.core.persistence.dao.RecordRepository;
import com.oots.core.persistence.dao.RoleRepository;
import com.oots.core.persistence.dao.UserRepository;
import com.oots.core.persistence.model.Privilege;
import com.oots.core.persistence.model.Role;
import com.oots.core.persistence.model.User;
import com.oots.core.persistence.model.enums.CivilState;
import com.oots.core.persistence.model.enums.FamilySide;
import com.oots.core.persistence.model.enums.Gender;
import com.oots.core.persistence.model.enums.HospitalizationType;
import com.oots.core.persistence.model.enums.HousingComponents;
import com.oots.core.persistence.model.enums.HousingMaterial;
import com.oots.core.persistence.model.enums.HousingOwnership;
import com.oots.core.persistence.model.enums.HousingType;
import com.oots.core.persistence.model.enums.IncomeEstimated;
import com.oots.core.persistence.model.enums.Medicare;
import com.oots.core.persistence.model.enums.RecordStatus;
import com.oots.core.persistence.model.enums.Scholarity;
import com.oots.core.persistence.model.enums.Treatment;
import com.oots.core.persistence.model.enums.UserStatus;
import com.oots.core.persistence.model.patient.AcademicHistory;
import com.oots.core.persistence.model.patient.Address;
import com.oots.core.persistence.model.patient.Condition;
import com.oots.core.persistence.model.patient.ContactInformation;
import com.oots.core.persistence.model.patient.Education;
import com.oots.core.persistence.model.patient.EmergencyContact;
import com.oots.core.persistence.model.patient.Family;
import com.oots.core.persistence.model.patient.FamilyCondition;
import com.oots.core.persistence.model.patient.FamilyMember;
import com.oots.core.persistence.model.patient.FullName;
import com.oots.core.persistence.model.patient.Guardian;
import com.oots.core.persistence.model.patient.Hospitalization;
import com.oots.core.persistence.model.patient.Housing;
import com.oots.core.persistence.model.patient.Income;
import com.oots.core.persistence.model.patient.Insurance;
import com.oots.core.persistence.model.patient.Job;
import com.oots.core.persistence.model.patient.Medicine;
import com.oots.core.persistence.model.patient.MentalHealth;
import com.oots.core.persistence.model.patient.Patient;
import com.oots.core.persistence.model.patient.PhysicalHealth;
import com.oots.core.persistence.model.patient.PsychiatricTreatment;
import com.oots.core.persistence.model.record.Record;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = true;
    
    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;
    
    @Autowired
    private PatientRepository patientRepository;
    
    @Autowired
    private RecordRepository recordRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {    	
        if (alreadySetup) {
            return;
        }

        // == create initial privileges
        final Privilege readPrivilege = createPrivilegeIfNotFound("READ_RECORD_PRIVILEGE");
        final Privilege writePrivilege = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
        final Privilege passwordPrivilege = createPrivilegeIfNotFound("CHANGE_PASSWORD_PRIVILEGE");

        // == create initial roles
        final List<Privilege> adminPrivileges = new ArrayList<Privilege>(Arrays.asList(readPrivilege, writePrivilege, passwordPrivilege));
        final List<Privilege> userPrivileges = new ArrayList<Privilege>(Arrays.asList(readPrivilege, passwordPrivilege));
        final Role adminRole = createRoleIfNotFound("ROLE_SUPERVISOR", adminPrivileges);
        final Role userRole = createRoleIfNotFound("ROLE_SOCIAL_WORKER", userPrivileges);

        // == create initial user
        User admin = createUserIfNotFound("admin@test.com", "Test", "Test", "test", new ArrayList<Role>(Arrays.asList(adminRole)));
        User user = createUserIfNotFound("user@test.com", "Test", "Test", "test", new ArrayList<Role>(Arrays.asList(userRole)));

        createPatient(admin, user);

        alreadySetup = true;
    }

    @Transactional
    private final Privilege createPrivilegeIfNotFound(final String name) {
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilege = privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    private final Role createRoleIfNotFound(final String name, final List<Privilege> privileges) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
        }
        role.setPrivileges(privileges);
        role = roleRepository.save(role);
        return role;
    }

    @Transactional
    private final User createUserIfNotFound(final String email, final String firstName, final String lastName, final String password, final List<Role> roles) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
        	// TODO: Fix this section
        	FullName name = new FullName(firstName, "", lastName, "", "");
            user = new User(name, email, password, UserStatus.ACTIVE, roles);
            user.setPassword(passwordEncoder.encode(password));
            user.setEmail(email);
        }
        user.setRoles(roles);
        user = userRepository.save(user);
        return user;
    }
    
    @Transactional
    private final void createPatient(User admin, User user) {
    	// Name
    	FullName name = new FullName("Eliezer", "", "Ferra", "Otero", "Elio");
    	
    	//DoB
    	DateTime dob = new DateTime(1989, 1, 17, 0, 0, 0);
    	
    	// Guardian
    	FullName guardianName = new FullName("Maria", "Caridad", "Otero", "Arroyo", "");
    	Address guardianAddress = new Address("calle 41 SE", "", "", "San Juan", "", "Puerto Rico", "00921");
    	ContactInformation guardianContact = new ContactInformation(guardianAddress, guardianAddress, "7873464875", "", "", "");
    	Guardian guardian = new Guardian(guardianName, guardianContact);
    	
    	// Contact Info
    	Address postal = new Address("200 Ave Los Chalets Apt 36", "Cond Chalets de Cupey", "", "San Juan", "", "Puerto Rico", "00926");
    	Address residential = new Address("200 Ave Los Chalets Apt E-409", "Cond Chalets de Cupey", "", "San Juan", "", "Puerto Rico", "00926");
    	ContactInformation contactInfo = new ContactInformation(residential, postal, "", "7873464257", "7877599999", "eliezer.ferra@gmail.com");
    	
    	// Housing
    	Housing housing = new Housing("This is a description", HousingType.APARTMENT, "", HousingMaterial.CONCRETE, "", "3", "2", Arrays.asList(HousingComponents.DINING_ROOM, HousingComponents.LAUNDRY_ROOM), HousingOwnership.RENT, "");
    	
    	// Job
    	Job job = new Job("Evertec", "Product Manager", "2 años", false, "");
    	
    	// Income
    	Income income = new Income();
    	income.setMontlyIncomeEstimate(IncomeEstimated.MORE_5000);
    	income.setMontlyIncome(55000);
    	
    	// Emergency Contact
    	FullName emergencyName = new FullName("Iris", "Yanice", "Rivera", "Torres", "");
    	Address emergencyPostal = new Address("200 Ave Los Chalets Apt 36", "Cond Chalets de Cupey", "", "San Juan", "", "Puerto Rico", "00926");
    	ContactInformation emergencyContactInfo = new ContactInformation(emergencyPostal, emergencyPostal, "", "7872353428", "", "i.yaniice@gmail.com");
    	EmergencyContact emergencyContact = new EmergencyContact(emergencyName, emergencyContactInfo, "wife");
    	
    	// Family
    	FamilyMember member1 = new FamilyMember("Iris Yanice Rivera", 27, "wife", Scholarity.ASSOCIATES, "");
    	FamilyMember member2 = new FamilyMember("Nephtali Ferra Santiago", 16, "nephew", Scholarity.HIGH_SCHOOL, "");
    	Family family = new Family("This is a small history", Arrays.asList(member1, member2));
    	
    	// Mental Health
    	Medicine medicine = new Medicine("Fliorizet", "2 tabletas", "cada 4 horas");
    	Condition condition = new Condition("Migraña", Treatment.YES, Arrays.asList(medicine, medicine, medicine, medicine));
    	FamilyCondition familyCondition = new FamilyCondition("Diabetes", FamilySide.MATERNAL);
    	Hospitalization hosp = new Hospitalization(new Date(), "Institucion", HospitalizationType.INSTITUTIONAL, "idk");
    	PsychiatricTreatment psychiatricTreatment = new PsychiatricTreatment("Dra. Rivera", "Psicologa Clínica", "7872353428", "200 ave los chalets");
    	MentalHealth mentalHealth = new MentalHealth("History", Arrays.asList(condition), psychiatricTreatment, Arrays.asList(familyCondition), true, 1, Arrays.asList(hosp));
    	
    	// Physical Health
    	Medicine medicine1 = new Medicine("Zanax", "2 tabletas", "cada 4 horas");
    	Condition condition1 = new Condition("Loquera", Treatment.YES, Arrays.asList(medicine1, medicine1, medicine1, medicine1));
    	Condition condition2 = new Condition("Loquera", Treatment.YES, Arrays.asList(medicine1, medicine1, medicine1, medicine1));
    	FamilyCondition familyCondition1 = new FamilyCondition("Depresion", FamilySide.MATERNAL);
    	Hospitalization hosp1 = new Hospitalization(new Date(), "Auxilio Mutuo", HospitalizationType.NA, "idk");
    	Hospitalization operation = new Hospitalization(new Date(), "Cardiovascular", HospitalizationType.NA, "idk");
    	PhysicalHealth physicalHealth = new PhysicalHealth("History", Arrays.asList(condition1, condition2), Arrays.asList(familyCondition1), true, 1, Arrays.asList(hosp1), true, 1, Arrays.asList(operation));
    	
    	// Insurance
    	Insurance insurance = new Insurance("SSS", Medicare.NOT_APPLY, "", new Date());
    	
    	// Academic History
    	Education education = new Education(Scholarity.BACHELOR, "Comp Sci", "Universidad Interamericana de Puerto Rico");
    	AcademicHistory academicHistory = new AcademicHistory( "Academic History", Arrays.asList(education));
    	
    	
    	Patient patient = new Patient(name, dob.toDate(), Gender.MALE, CivilState.MARRIED, "CMA", contactInfo, academicHistory, job, income, insurance, housing, guardian, emergencyContact, family, physicalHealth, mentalHealth);
    	Patient newPatient = patientRepository.save(patient);
    	
    	Record recordUser = new Record();
    	recordUser.setPatient(newPatient);
    	recordUser.setStatus(RecordStatus.OPEN);
    	recordUser.setUser(user);
    	
    	Record recordAdmin = new Record();
    	recordAdmin.setPatient(newPatient);
    	recordAdmin.setStatus(RecordStatus.OPEN);
    	recordAdmin.setUser(admin);
    	
    	Record newRecordUser = recordRepository.save(recordUser);
    	Record newRecordAdmin = recordRepository.save(recordAdmin);
    }
}