package com.oots.web.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.oots.core.persistence" })
@EnableJpaRepositories(basePackages = "com.oots.core.persistence.dao")
public class PersistenceJPAConfig {

    public PersistenceJPAConfig() {
        super();
    }
}
