package com.oots.web.dto;

import com.oots.web.dto.patient.FullNameDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientSearch {
	private FullNameDTO name; 
}
