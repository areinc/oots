package com.oots.web.dto;

import com.oots.core.persistence.model.enums.Roles;
import com.oots.web.dto.patient.FullNameDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
	private Long id;
    private FullNameDTO name;
    private String password;
    private String matchingPassword;
    private String email;
    private Roles role;
}
