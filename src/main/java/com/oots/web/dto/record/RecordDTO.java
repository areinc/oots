package com.oots.web.dto.record;

import java.util.List;

import com.oots.core.persistence.model.enums.RecordStatus;
import com.oots.web.dto.UserDTO;
import com.oots.web.dto.patient.PatientDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RecordDTO {
    private Long id;
	private UserDTO user;
	private RecordStatus status;
	private PatientDTO patient;
	private List<DocumentDTO> documents;
}
