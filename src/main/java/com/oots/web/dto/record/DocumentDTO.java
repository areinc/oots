package com.oots.web.dto.record;

import com.oots.core.persistence.model.enums.DocumentType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DocumentDTO {
	private Long id;
	private DocumentType documentType;
	private String fileType;
	private byte[] file;
}
