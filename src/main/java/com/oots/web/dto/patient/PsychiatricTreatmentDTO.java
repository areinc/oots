package com.oots.web.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PsychiatricTreatmentDTO {
	private String doctor;
	private String specialty;
	private String phone;
	private String address;
}
