package com.oots.web.dto.patient;

import com.oots.core.persistence.model.enums.Scholarity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FamilyMemberDTO {
	private String name;
	private int age;
	private String relationship;
	private Scholarity scholarity;
	private String occupation;
}
