package com.oots.web.dto.patient;

import com.oots.core.persistence.model.enums.Medicare;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InsuranceDTO {
	private String name;
	private Medicare medicare;
	private String complementary;
	private String expDate;
}
