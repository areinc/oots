package com.oots.web.dto.patient;

import com.oots.core.persistence.model.enums.HospitalizationType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HospitalizationDTO {
	private String date;
	private String institution;
	private HospitalizationType type;
	private String reason;
}
