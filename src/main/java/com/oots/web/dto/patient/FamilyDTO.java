package com.oots.web.dto.patient;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FamilyDTO {
	private String history;
	private List<FamilyMemberDTO> familyMembers = new ArrayList<FamilyMemberDTO>();
}
