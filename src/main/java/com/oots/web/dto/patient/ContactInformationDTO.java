package com.oots.web.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContactInformationDTO {
	private AddressDTO residentialAddress = new AddressDTO();
	private AddressDTO postalAddress = new AddressDTO();
	private String homePhone;
	private String mobilePhone;
	private String workPhone;
	private String email;
}
