package com.oots.web.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FullNameDTO {
	private String firstName;
	private String middleName;
	private String lastName;
	private String maidenName;
	private String nickname;
	
	public String getFullName() {
		return firstName + " " + middleName + " " + lastName + " " + maidenName;
	}
}
