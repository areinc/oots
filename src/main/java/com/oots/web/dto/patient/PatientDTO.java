package com.oots.web.dto.patient;

import java.util.ArrayList;
import java.util.List;

import com.oots.core.persistence.model.enums.CivilState;
import com.oots.core.persistence.model.enums.Gender;
import com.oots.web.dto.record.RecordDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientDTO {
	private Long id;
	private FullNameDTO name = new FullNameDTO();
	private String dateOfBirth;
	private Gender gender;
	private CivilState civilState;
	private String religion;
	private ContactInformationDTO contactInfo = new ContactInformationDTO();
	private AcademicHistoryDTO academicHistory = new AcademicHistoryDTO();
	private JobDTO job = new JobDTO();
	private IncomeDTO income = new IncomeDTO();
	private InsuranceDTO insurance = new InsuranceDTO();
	private HousingDTO housing = new HousingDTO();
	private GuardianDTO guardian = new GuardianDTO();
	private EmergencyContactDTO emergencyContact = new EmergencyContactDTO();
	private FamilyDTO family = new FamilyDTO();
	private PhysicalHealthDTO physicalHealth = new PhysicalHealthDTO();
	private MentalHealthDTO mentalHealth = new MentalHealthDTO();
	private List<RecordDTO> records = new ArrayList<RecordDTO>();
}
