package com.oots.web.dto.patient;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AcademicHistoryDTO {
	private String history;
	private List<EducationDTO> education = new ArrayList<EducationDTO>();
}
