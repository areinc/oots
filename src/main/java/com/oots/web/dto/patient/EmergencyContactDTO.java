package com.oots.web.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmergencyContactDTO {
	private FullNameDTO name = new FullNameDTO();
	private ContactInformationDTO contactInfo = new ContactInformationDTO();
	private String relationship;
}
