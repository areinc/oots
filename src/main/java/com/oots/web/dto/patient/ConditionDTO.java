package com.oots.web.dto.patient;

import java.util.ArrayList;
import java.util.List;

import com.oots.core.persistence.model.enums.Treatment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConditionDTO {
	private String name;
	private Treatment treatment;
	private List<MedicineDTO> medicine = new ArrayList<MedicineDTO>();
}
