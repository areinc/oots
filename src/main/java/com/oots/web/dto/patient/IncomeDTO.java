package com.oots.web.dto.patient;

import com.oots.core.persistence.model.enums.IncomeEstimated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IncomeDTO {
	private IncomeEstimated montlyIncomeEstimate;
	private int montlyIncome;
	private String salary;
	private String socialSecurity;
	private String pension;
	private String ebt;
	private String childSupport;
	private String welness;
	private String rent;
	private String unemployment;
	private String business;
	private String other;
}
