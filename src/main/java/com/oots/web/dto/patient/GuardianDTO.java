package com.oots.web.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GuardianDTO {
	private FullNameDTO name = new FullNameDTO();
	private ContactInformationDTO contactInfo = new ContactInformationDTO();
}
