package com.oots.web.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO {
	private String firstLine;
	private String secondLine;
	private String thirdLine;
	private String city;
	private String state;
	private String country;
	private String postalCode;
}
