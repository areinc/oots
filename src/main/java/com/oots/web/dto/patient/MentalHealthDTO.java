package com.oots.web.dto.patient;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MentalHealthDTO {
	private String history;
	private List<ConditionDTO> conditions = new ArrayList<ConditionDTO>();
	private PsychiatricTreatmentDTO psychiatricTreatment = new PsychiatricTreatmentDTO();
	private List<FamilyConditionDTO> familyHistory = new ArrayList<FamilyConditionDTO>();
	private boolean hospitalized;
	private int amountOfHospitalizations;
	private List<HospitalizationDTO> hospitalizations = new ArrayList<HospitalizationDTO>();
}
