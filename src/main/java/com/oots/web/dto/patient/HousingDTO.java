package com.oots.web.dto.patient;

import java.util.ArrayList;
import java.util.List;

import com.oots.core.persistence.model.enums.HousingComponents;
import com.oots.core.persistence.model.enums.HousingMaterial;
import com.oots.core.persistence.model.enums.HousingOwnership;
import com.oots.core.persistence.model.enums.HousingType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HousingDTO {
	private String description;
	private HousingType type;
	private String otherType;
	private HousingMaterial materials;
	private String otherMaterials;
	private String bedrooms;
	private String bathrooms;
	private List<HousingComponents> components = new ArrayList<HousingComponents>();
	private String otherComponents;
	private HousingOwnership ownership;
	private String otherOwnership;
}
