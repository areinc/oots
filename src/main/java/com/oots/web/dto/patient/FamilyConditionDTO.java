package com.oots.web.dto.patient;

import com.oots.core.persistence.model.enums.FamilySide;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FamilyConditionDTO {
	private String condition;
	private FamilySide familySide;
}
