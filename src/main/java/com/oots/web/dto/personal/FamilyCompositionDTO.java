package com.oots.web.dto.personal;

import java.util.ArrayList;
import java.util.List;

public class FamilyCompositionDTO {
	private String familyHistory;
	private List<FamilyMemberDTO> family;
	
	public FamilyCompositionDTO() {
		super();
		family = new ArrayList<FamilyMemberDTO>();
	}
	public String getFamilyHistory() {
		return familyHistory;
	}
	public void setFamilyHistory(String familyHistory) {
		this.familyHistory = familyHistory;
	}
	public List<FamilyMemberDTO> getFamily() {
		return family;
	}
	public void setFamily(List<FamilyMemberDTO> family) {
		this.family = family;
	}
}
