package com.oots.web.dto.personal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.oots.core.persistence.model.enums.CivilState;
import com.oots.core.persistence.model.enums.Gender;
import com.oots.core.persistence.model.enums.HousingComponents;
import com.oots.core.persistence.model.enums.HousingMaterial;
import com.oots.core.persistence.model.enums.HousingOwnership;
import com.oots.core.persistence.model.enums.HousingType;
import com.oots.core.persistence.model.enums.IncomeEstimated;
import com.oots.core.persistence.model.enums.Medicare;
import com.oots.web.dto.patient.FullNameDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonalInformationDTO {
	// Personal Information
	private FullNameDTO name;
	private String dateOfBirth; 
	private Gender gender;
	private CivilState civilState;
	private String religion;
	private String tutorName;
	private String tutorPhone;
	
	//Contact Information
	private String mainPhone;
	private String mobilePhone;
	private String workPhone;
	private String email;
	private String postalAddress;
	private String postalState;
	private String postalCountry;
	private String postalZipcode;
	private String physicalAddress;
	private String physicalState;
	private String physicalCountry;
	private String physicalZipcode;
	
	// Housing Information
	private String housing;
	private HousingType housingType;
	private String otherHousingType;
	private HousingMaterial housingMaterial;
	private String otherHousingMaterial;
	private String bedrooms;
	private String bathroms;
	private List<HousingComponents> housingComponents;
	private String otherHousingComponents;
	private HousingOwnership housingOwnership;
	private String otherHousingOwnership;
	
	// Education information
	private String academicHistory;
	private List<ScholarityDTO> education;
	
	// Work information
	private String placeOfWork;
	private String profession;
	private String timeOfEmployment;
	private boolean hadDiffcultiesAtWork;
	private String difficultiesAtWork;
	
	// Medical information
	private String medicalInsurance;
	private Medicare medicare;
	private String medicareComplementary;
	private Date insuranceExpiration;
	
	private IncomeEstimated estimatedIncome;
	private List<IncomeDTO> incomeSources;
	
	private String ecFirstName;
	private String ecMiddleName;
	private String ecLastName;
	private String ecMaidenName;
	private String ecRelationship;
	private String ecMainPhone;
	private String ecMobilePhone;
	private String ecWorkPhone;
	private String ecAddress;
	private String ecState;
	private String ecCountry;
	private String ecZipcode;
	
	public PersonalInformationDTO() {
		super();
		housingComponents = new ArrayList<HousingComponents>();
		education = new ArrayList<ScholarityDTO>();
		incomeSources = new ArrayList<IncomeDTO>();
	}	
}
