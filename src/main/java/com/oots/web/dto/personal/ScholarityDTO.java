package com.oots.web.dto.personal;

import com.oots.core.persistence.model.enums.Scholarity;

public class ScholarityDTO {
	private Scholarity scholarity = Scholarity.NONE;
	private String concentration = "";
	private String school = "";
	
	public Scholarity getScholarity() {
		return scholarity;
	}
	public void setScholarity(Scholarity scholarity) {
		this.scholarity = scholarity;
	}
	public String getConcentration() {
		return concentration;
	}
	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
}
