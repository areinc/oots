package com.oots.web.dto.personal;

import com.oots.core.persistence.model.enums.IncomeSources;

public class IncomeDTO {
	private IncomeSources type;
	private String amount;
	
	public IncomeSources getType() {
		return type;
	}
	public void setType(IncomeSources type) {
		this.type = type;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
}
