package com.oots.web.dto.personal;

public class FamilyMemberDTO {
	private String name;
	private String age;
	private String relationship;
	private String scholarity;
	private String ocupation;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getScholarity() {
		return scholarity;
	}
	public void setScholarity(String scholarity) {
		this.scholarity = scholarity;
	}
	public String getOcupation() {
		return ocupation;
	}
	public void setOcupation(String ocupation) {
		this.ocupation = ocupation;
	}
}
