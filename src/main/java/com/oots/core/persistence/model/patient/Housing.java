package com.oots.core.persistence.model.patient;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.oots.core.persistence.model.enums.HousingComponents;
import com.oots.core.persistence.model.enums.HousingMaterial;
import com.oots.core.persistence.model.enums.HousingOwnership;
import com.oots.core.persistence.model.enums.HousingType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class Housing {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String description = "";
	
	@Enumerated(EnumType.STRING)
	private HousingType type;
	
	@Column(name="other_type")
	private String otherType = "";
	
	@Enumerated(EnumType.STRING)
	private HousingMaterial materials;
	
	@Column(name="other_materials")
	private String otherMaterials = "";
	
	private String bedrooms = "";
	
	private String bathrooms = "";
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private List<HousingComponents> components;
	
	private String otherComponents = "";
	
	@Enumerated(EnumType.STRING)
	private HousingOwnership ownership;
	
	@Column(name="other_ownership")
	private String otherOwnership = "";

	public Housing(String description, HousingType type, String otherType, HousingMaterial materials,
			String otherMaterials, String bedrooms, String bathrooms, List<HousingComponents> components,
			HousingOwnership ownership, String otherOwnership) {
		super();
		this.description = description;
		this.type = type;
		this.otherType = otherType;
		this.materials = materials;
		this.otherMaterials = otherMaterials;
		this.bedrooms = bedrooms;
		this.bathrooms = bathrooms;
		this.components = components;
		this.ownership = ownership;
		this.otherOwnership = otherOwnership;
	}
}
