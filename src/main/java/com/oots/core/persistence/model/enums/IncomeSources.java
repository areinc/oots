package com.oots.core.persistence.model.enums;

public enum IncomeSources {
	SALARY("Sueldo"),
	SOCIAL_SECURITY("Seguro Social"),
	PENSION("Pensi&oacute;n"),
	EBT("PAN"),
	CHILD_SUPPORT("Pensi&oacute;n alimentaria"),
	RENT("Rentas"),
	UNEMPLOYMENT("Desempleo"),
	OWN_BUSINESS("Negocio propio"),
	OTHER ("Otro");
	
	private String desc;
	IncomeSources(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
