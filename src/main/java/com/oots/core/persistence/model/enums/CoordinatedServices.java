package com.oots.core.persistence.model.enums;

public enum CoordinatedServices {
	DEPT_FAMILIA("Departamento de la Familia"),
	VIVIENDA("Vivienda"),
	SALUD("Salud"),
	SALUD_MENTAL("Salud Mental"),
	CONGREGACION_MITA("Congregacion Mita");
	
	private String desc;
	CoordinatedServices(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
