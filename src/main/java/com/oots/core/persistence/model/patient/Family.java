package com.oots.core.persistence.model.patient;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.vladmihalcea.hibernate.type.json.JsonStringType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "family_composition")
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class Family {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String history = "";
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private List<FamilyMember> familyMembers = new ArrayList<FamilyMember>();

	public Family(String history, List<FamilyMember> familyMembers) {
		super();
		this.history = history;
		this.familyMembers = familyMembers;
	}
}
