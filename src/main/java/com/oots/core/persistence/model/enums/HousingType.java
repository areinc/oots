package com.oots.core.persistence.model.enums;

public enum HousingType {
	ONE_STORY("Casa de una planta"),
	TWO_STORY("Casa de dos plantas"),
	STUDIO ("Estudio"),
	APARTMENT ("Apartamento"),
	OTHER ("Otro");
	
	private String desc;
	HousingType(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
