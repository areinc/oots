package com.oots.core.persistence.model.patient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Address {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name="first_line")
	private String firstLine = "";
	
	@Column(name="second_line")
	private String secondLine = "";
	
	@Column(name="third_line")
	private String thirdLine = "";
	
	private String city = "";
	
	private String state = "";
	
	private String country = "";
	
	@Column(name="postal_code")
	private String postalCode = "";

	public Address(String firstLine, String secondLine, String thirdLine, String city, String state, String country,
			String postalCode) {
		super();
		this.firstLine = firstLine;
		this.secondLine = secondLine;
		this.thirdLine = thirdLine;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
	}
}
