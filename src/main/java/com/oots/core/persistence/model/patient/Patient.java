package com.oots.core.persistence.model.patient;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.TypeDef;

import com.oots.core.persistence.model.enums.CivilState;
import com.oots.core.persistence.model.enums.Gender;
import com.oots.core.persistence.model.record.Record;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@TypeDef(name = "json", typeClass = JsonStringType.class)
@Entity
public class Patient {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "full_name_id", referencedColumnName = "id")
	private FullName name = new FullName();
	
	private Date dateOfBirth;
	
	@Enumerated(EnumType.STRING)
	private Gender gender;
	
	@Enumerated(EnumType.STRING)
	private CivilState civilState;
	
	private String religion = "";

	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "contact_information_id", referencedColumnName = "id")
	private ContactInformation contactInfo = new ContactInformation();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "academic_history_id", referencedColumnName = "id")
	private AcademicHistory academicHistory = new AcademicHistory();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "job_id", referencedColumnName = "id")
	private Job job = new Job();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "income_id", referencedColumnName = "id")
	private Income income = new Income();

	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "insurance_id", referencedColumnName = "id")
	private Insurance insurance = new Insurance();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "housing_id", referencedColumnName = "id")
	private Housing housing = new Housing();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "guardian_id", referencedColumnName = "id")
	private Guardian guardian = new Guardian();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "emergency_contact_id", referencedColumnName = "id")
	private EmergencyContact emergencyContact = new EmergencyContact();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "family_composition_id", referencedColumnName = "id")
	private Family family = new Family();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "physical_health_id", referencedColumnName = "id")
	private PhysicalHealth physicalHealth = new PhysicalHealth();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "mental_health_id", referencedColumnName = "id")
	private MentalHealth mentalHealth = new MentalHealth();
	
	@OneToMany(mappedBy="patient", cascade = CascadeType.ALL)
	private List<Record> records;

	public Patient(FullName name, Date dateOfBirth, Gender gender, CivilState civilState, String religion,
			ContactInformation contactInfo, AcademicHistory academicHistory, Job job, Income income,
			Insurance insurance, Housing housing, Guardian guardian, EmergencyContact emergencyContact, Family family,
			PhysicalHealth physicalHealth, MentalHealth mentalHealth) {
		super();
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.civilState = civilState;
		this.religion = religion;
		this.contactInfo = contactInfo;
		this.academicHistory = academicHistory;
		this.job = job;
		this.income = income;
		this.insurance = insurance;
		this.housing = housing;
		this.guardian = guardian;
		this.emergencyContact = emergencyContact;
		this.family = family;
		this.physicalHealth = physicalHealth;
		this.mentalHealth = mentalHealth;
	}
}
