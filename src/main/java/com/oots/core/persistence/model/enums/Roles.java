package com.oots.core.persistence.model.enums;

public enum Roles {
	SUPERVISOR("Supervisor"),
	SOCIAL_WORKER("Trabajadora Social"),
	ADMINISTRATIVE_ASSITANT("Asistente Administrativa"),
	PSYCOLOGIS("Psycologa");
	
	private String desc;
	Roles(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
