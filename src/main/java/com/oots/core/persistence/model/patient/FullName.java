package com.oots.core.persistence.model.patient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class FullName {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name="first_name")
	private String firstName = "";
	
	@Column(name="middle_name")
	private String middleName = "";
	
	@Column(name="last_name")
	private String lastName = "";
	
	@Column(name="maiden_name")
	private String maidenName = "";
	
	private String nickname = "";
	
	public String getFullName() {
		return firstName + " " + middleName + " " + lastName + " " + maidenName;
	}

	public FullName(String firstName, String middleName, String lastName, String maidenName, String nickname) {
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.maidenName = maidenName;
		this.nickname = nickname;
	}
}
