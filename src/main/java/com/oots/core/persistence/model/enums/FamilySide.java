package com.oots.core.persistence.model.enums;

public enum FamilySide {
	PATERNAL("Paterno"),
	MATERNAL("Materno");
	
	private String desc;
	FamilySide(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
