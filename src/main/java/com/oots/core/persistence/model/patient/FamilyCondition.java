package com.oots.core.persistence.model.patient;

import java.io.Serializable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.oots.core.persistence.model.enums.FamilySide;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("serial")
public class FamilyCondition implements Serializable {
	private String condition = "";
	
	@Enumerated(EnumType.STRING)
	private FamilySide familySide;
}
