package com.oots.core.persistence.model.patient;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ContactInformation {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "residential_address_id", referencedColumnName = "id")
	private Address residentialAddress = new Address();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "postal_address_id",referencedColumnName = "id")
	private Address postalAddress = new Address();
	
	private String homePhone = "";
	
	private String mobilePhone = "";
	
	private String workPhone = "";
	
	private String email = "";
	
	public ContactInformation(Address residentialAddress, Address postalAddress, String homePhone, String mobilePhone,
			String workPhone, String email) {
		super();
		this.residentialAddress = residentialAddress;
		this.postalAddress = postalAddress;
		this.homePhone = homePhone;
		this.mobilePhone = mobilePhone;
		this.workPhone = workPhone;
		this.email = email;
	}
}
