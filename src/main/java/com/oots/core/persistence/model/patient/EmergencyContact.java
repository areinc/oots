package com.oots.core.persistence.model.patient;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class EmergencyContact {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "full_name_id", referencedColumnName = "id")
	private FullName name = new FullName();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "contact_information_id",referencedColumnName = "id")
	private ContactInformation contactInfo = new ContactInformation();
	
	private String relationship = "";

	public EmergencyContact(FullName name, ContactInformation contactInfo, String relationship) {
		super();
		this.name = name;
		this.contactInfo = contactInfo;
		this.relationship = relationship;
	}
}
