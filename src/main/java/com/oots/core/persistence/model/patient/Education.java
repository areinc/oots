package com.oots.core.persistence.model.patient;

import java.io.Serializable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.oots.core.persistence.model.enums.Scholarity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class Education implements Serializable {
	@Enumerated(EnumType.STRING)
	private Scholarity scholarity;
	
	private String concentration = "";
	
	private String school = "";
}
