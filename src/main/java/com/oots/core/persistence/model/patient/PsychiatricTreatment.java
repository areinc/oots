package com.oots.core.persistence.model.patient;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("serial")
public class PsychiatricTreatment implements Serializable {
	private String doctor = "";
	private String specialty = "";
	private String phone = "";
	private String address = "";
}
