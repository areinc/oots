package com.oots.core.persistence.model.patient;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

import com.oots.core.persistence.model.enums.IncomeEstimated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Income {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "monthly_income_estimate")
	private IncomeEstimated montlyIncomeEstimate;
	
	@Column(name = "monthly_income")
	private int montlyIncome = 0;
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private List<IncomeSource> sources = new ArrayList<IncomeSource>();

	public Income(IncomeEstimated montlyIncomeEstimate, int montlyIncome, List<IncomeSource> sources) {
		super();
		this.montlyIncomeEstimate = montlyIncomeEstimate;
		this.montlyIncome = montlyIncome;
		this.sources = sources;
	}
}
