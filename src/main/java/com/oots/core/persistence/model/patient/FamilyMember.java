package com.oots.core.persistence.model.patient;

import java.io.Serializable;

import com.oots.core.persistence.model.enums.Scholarity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class FamilyMember implements Serializable {
	private String name = "";
	
	private int age = 0;
	
	private String relationship = "";
	
	private Scholarity scholarity;
	
	private String occupation = "";
}
