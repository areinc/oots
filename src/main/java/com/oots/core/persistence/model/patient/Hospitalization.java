package com.oots.core.persistence.model.patient;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.oots.core.persistence.model.enums.HospitalizationType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("serial")
public class Hospitalization implements Serializable{
	private Date date;
	
	private String institution = "";
	
	@Enumerated(EnumType.STRING)
	private HospitalizationType type;
	
	private String reason = "";
}
