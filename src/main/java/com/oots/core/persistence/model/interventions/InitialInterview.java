package com.oots.core.persistence.model.interventions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.oots.core.persistence.model.enums.CoordinatedServices;
import com.oots.core.persistence.model.enums.ProblemsIdentified;
import com.oots.core.persistence.model.record.Record;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class InitialInterview {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private Date date;
	
	private String motive;
	
	private String referalSource;
	
	private String referalSourcePhone;
	
	private String referalMotive;
	
	private String otherServicesAgencies;
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private List<ProblemsIdentified> problemsIdentified = new ArrayList<ProblemsIdentified>();
	
	private String otherProblems;
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private List<CoordinatedServices> coordinatedServices = new ArrayList<CoordinatedServices>();
	
	private String otherServices;
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private List<Modification> modifications;
	
	@ManyToOne
	@JoinColumn(name="record_id", nullable = false)
	private Record record;
}
