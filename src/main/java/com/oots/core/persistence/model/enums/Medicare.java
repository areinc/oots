package com.oots.core.persistence.model.enums;

public enum Medicare {
	PART_A("Parte A"),
	PART_B("Parte B"),
	PART_C("Parte C"),
	COMPLEMENTARY("Complementario"),
	NOT_APPLY("No Aplica");
	
	private String desc;
	Medicare(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
