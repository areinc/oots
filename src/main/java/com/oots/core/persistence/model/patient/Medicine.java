package com.oots.core.persistence.model.patient;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class Medicine implements Serializable{
	private String name = "";
	
	private String amount = "";
	
	private String dose = "";
}
