package com.oots.core.persistence.model.patient;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.vladmihalcea.hibernate.type.json.JsonStringType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class MentalHealth {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String history = "";
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private List<Condition> conditions = new ArrayList<Condition>();
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private PsychiatricTreatment psychiatricTreatment = new PsychiatricTreatment();
	
	@Type(type="json")
	@Column(name="family_history", columnDefinition = "json")
	private List<FamilyCondition> familyHistory = new ArrayList<FamilyCondition>();
	
	private boolean hospitalized = false;
	
	@Column(name="amount_hospitalizations")
	private int amountOfHospitalizations = 0;
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private List<Hospitalization> hospitalizations = new ArrayList<Hospitalization>();

	public MentalHealth(String history, List<Condition> conditions, PsychiatricTreatment psychiatricTreatment, List<FamilyCondition> familyHistory,
			boolean hospitalized, int amountOfHospitalizations, List<Hospitalization> hospitalizations) {
		super();
		this.history = history;
		this.conditions = conditions;
		this.psychiatricTreatment = psychiatricTreatment;
		this.familyHistory = familyHistory;
		this.hospitalized = hospitalized;
		this.amountOfHospitalizations = amountOfHospitalizations;
		this.hospitalizations = hospitalizations;
	}
}
