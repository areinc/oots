package com.oots.core.persistence.model.enums;

public enum Scholarity {
	ELEMENTARY_SCHOOL("Escuela elemental"),
	MIDDLE_SCHOOL("Escuela intermedia"),
	HIGH_SCHOOL("Escuela superiore"),
	ASSOCIATES("Grado asociado"),
	BACHELOR("Bachillerato"),
	MASTERS("Maestria"),
	DOCTORATE("Doctorado"),
	NONE("Ninguna"),
	OTHER("Otra");
	
	private String desc;
	Scholarity(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
