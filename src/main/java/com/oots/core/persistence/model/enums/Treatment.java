package com.oots.core.persistence.model.enums;

public enum Treatment {
	YES("Si"),
	NO("No"),
	UNKNOW("Se desconoce");
	
	private String desc;
	Treatment(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
