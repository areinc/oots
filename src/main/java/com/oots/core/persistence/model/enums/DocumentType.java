package com.oots.core.persistence.model.enums;

public enum DocumentType {
	DISCLAIMER("Relevo de reponsibilidad"),
	CONFIDENTIALITY_PARAMETERS("Parametros de confidencialidad");
	
	private String desc;
	DocumentType(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
