package com.oots.core.persistence.model.enums;

public enum HousingOwnership {
	OWN("Propia"),
	RENT("Alquilada"),
	OTHER ("Otro");
	
	private String desc;
	HousingOwnership(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
