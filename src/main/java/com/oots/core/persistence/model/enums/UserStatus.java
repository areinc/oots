package com.oots.core.persistence.model.enums;

public enum UserStatus {
	ACTIVE("Activo"),
	LOCKED("Blockeado"),
	INACTIVE("Inactivo");
	
	private String desc;
	UserStatus(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
