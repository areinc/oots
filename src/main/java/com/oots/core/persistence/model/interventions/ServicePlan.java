package com.oots.core.persistence.model.interventions;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.oots.core.persistence.model.record.Record;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class ServicePlan {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String goals;
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private List<ServiceObjective> serviceObjectives = new ArrayList<ServiceObjective>();
	
	private String methods;
	
	private String referals;
	
	@Type(type="json")
	@Column(columnDefinition = "json")
	private List<Modification> modifications;
	
	@ManyToOne
	@JoinColumn(name="record_id", nullable = false)
	private Record record;
}
