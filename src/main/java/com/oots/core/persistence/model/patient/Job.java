package com.oots.core.persistence.model.patient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Job {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String company = "";
	
	private String position = "";
	
	private String time = "";
	
	@Column(name="had_difficulties")
	private boolean hadDifficulties = false;
	
	private String difficulties = "";
	
	public Job(String company, String position, String time, boolean hadDifficulties, String difficulties) {
		super();
		this.company = company;
		this.position = position;
		this.time = time;
		this.hadDifficulties = hadDifficulties;
		this.difficulties = difficulties;
	}
}
