package com.oots.core.persistence.model.enums;

public enum HousingComponents {
	LIVING_ROOM("Sala"),
	DINING_ROOM("Comerdor"),
	FAMILY_ROOM("Sala familiar"),
	LAUNDRY_ROOM("Cuarto de lavander&iacute;a"),
	MARQUEE ("Marquesina"),
	LIGHT_METER("Contador de Luz"),
	WATER_METER("Contador de Agua"),
	OTHER ("Otro");
	
	private String desc;
	HousingComponents(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
