package com.oots.core.persistence.model.patient;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.oots.core.persistence.model.enums.Medicare;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Insurance {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String name = "";
	
	@Enumerated(EnumType.STRING)
	private Medicare medicare;
	
	private String complementary = "";
	
	@Column(name="expiration_date")
	private Date expDate;

	public Insurance(String name, Medicare medicare, String complementary, Date expDate) {
		super();
		this.name = name;
		this.medicare = medicare;
		this.complementary = complementary;
		this.expDate = expDate;
	}
}
