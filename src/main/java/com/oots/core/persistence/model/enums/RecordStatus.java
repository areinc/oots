package com.oots.core.persistence.model.enums;

public enum RecordStatus {
	OPEN("Abierto"),
	CLOSED("Cerrado"),
	REOPENED("Reabierto");
	
	private String desc;
	RecordStatus(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
