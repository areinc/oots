package com.oots.core.persistence.model.enums;

public enum IncomeEstimated {
	UP_TO_100("De $0 - $100"),
	UP_TO_200("De $101 - $200"),
	UP_TO_300("De $201 - $300"),
	UP_TO_400("De $301 - $400"),
	UP_TO_500("De $401 - $500"),
	UP_TO_600("De $501 - $600"),
	UP_TO_700("De $601 - $700"),
	UP_TO_800("De $701 - $800"),
	UP_TO_900("De $801 - $900"),
	UP_TO_1000("De $901 - $1,000"),
	UP_TO_1500("De $1,001 - $1,500"),
	UP_TO_2000("De $1,501 - $2,000"),
	UP_TO_2500("De $2,001 - $2,500"),
	UP_TO_3000("De $2,501 - $3,000"),
	UP_TO_3500("De $3,001 - $3,500"),
	UP_TO_4000("De $3,501 - $4,000"),
	UP_TO_4500("De $4,001 - $4,500"),
	UP_TO_5000("De $4,501 - $5,000"),
	MORE_5000("Mas de $5,000");
	
	private String desc;
	IncomeEstimated(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
