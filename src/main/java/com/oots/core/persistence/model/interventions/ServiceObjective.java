package com.oots.core.persistence.model.interventions;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ServiceObjective implements Serializable{
	private String objective;
	private String strategies;
	private String techniques;
}
