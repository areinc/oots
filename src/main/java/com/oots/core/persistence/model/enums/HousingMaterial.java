package com.oots.core.persistence.model.enums;

public enum HousingMaterial {
	WOOD("Madera"),
	CONCRETE("Cemento"),
	WOOD_CONCRETE ("Madera y cemento"),
	OTHER ("Otro");
	
	private String desc;
	HousingMaterial(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
