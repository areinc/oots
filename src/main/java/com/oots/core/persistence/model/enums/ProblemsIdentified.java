package com.oots.core.persistence.model.enums;

public enum ProblemsIdentified {
	GENERAL_ORIENTATION("Orientación general"),
	HOUSING("Vivienda"),
	UNEMPLOYMENT("Falta de empleo"),
	LOW_SELF_ESTEEM("Baja autoestima"),
	STRESS("Estrés"),
	AGGRESSIVE_CONDUCT("Conducta agresiva"),
	DOMESTIC_VIOLENCE("Violencia en el hogar"),
	CHILD_ABUSE("Maltrato a menores"),
	TEEN_PREGNANCY("Embarazo adolescente"),
	DEPRESSION("Depresi&oacute;n"),
	PSYCHIATRIC_PROBLEMS("Problemas psiquiátricos"),
	FAMILY_PROBLEMS("Problemas familiares"),
	MARRIAGE_PROBLEMS("Conflictos maritales"),
	DIVORCE("Crisis de divorcio"),
	ALCOHOL_ABUSE("Abuso de alcohol"),
	DRUGS("Uso de drogas ilícitas"),
	FINANCIAL_PROBLEMS("Problemas financieros"),
	EATING_DISORDER("Des&oacute;rdenes alimenticios"),
	SPIRITUAL_PROBLEMS("Problemas espirituales"),
	SOCIAL_SECURITY("Seguro social"),
	MEDICARE("Problemas con Medicare"),
	GOVERNMENT("Problemas con alguna agencia gubernamental"),
	HEALT("Salud");
	
	private String desc;
	ProblemsIdentified(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
