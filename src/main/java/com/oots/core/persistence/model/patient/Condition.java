package com.oots.core.persistence.model.patient;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.oots.core.persistence.model.enums.Treatment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("serial")
public class Condition implements Serializable{
	private String name = "";
	
	@Enumerated(EnumType.STRING)
	private Treatment treatment;
	
	private List<Medicine> medicine = new ArrayList<Medicine>();
}
