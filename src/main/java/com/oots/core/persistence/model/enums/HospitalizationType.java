package com.oots.core.persistence.model.enums;

public enum HospitalizationType {
	PARTIAL("Parcial"),
	INSTITUTIONAL("Institucional"),
	NA ("N/A");
	
	private String desc;
	HospitalizationType(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
