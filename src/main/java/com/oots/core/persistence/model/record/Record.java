package com.oots.core.persistence.model.record;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.oots.core.persistence.model.User;
import com.oots.core.persistence.model.enums.RecordStatus;
import com.oots.core.persistence.model.interventions.CaseDiscussion;
import com.oots.core.persistence.model.interventions.ClosingReport;
import com.oots.core.persistence.model.interventions.InitialInterview;
import com.oots.core.persistence.model.interventions.ProgressNote;
import com.oots.core.persistence.model.interventions.ServicePlan;
import com.oots.core.persistence.model.interventions.TransferReport;
import com.oots.core.persistence.model.interventions.Weighing;
import com.oots.core.persistence.model.patient.Patient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Record {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable = false)
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="patient_id", nullable = false)
	private Patient patient;
	
	@Enumerated(EnumType.STRING)
	private RecordStatus status;
	
	@OneToMany(mappedBy="record")
	private List<Document> documents;
	
	@OneToMany(mappedBy="record", cascade = CascadeType.ALL)
	private List<InitialInterview> initialInterview;

	@OneToMany(mappedBy="record", cascade = CascadeType.ALL)
	private List<ProgressNote> progressNote;
	
	@OneToMany(mappedBy="record", cascade = CascadeType.ALL)
	private List<Weighing> weighing;
	
	@OneToMany(mappedBy="record", cascade = CascadeType.ALL)
	private List<ServicePlan> servicePlan;
	
	@OneToMany(mappedBy="record", cascade = CascadeType.ALL)
	private List<CaseDiscussion> caseDiscussions;
	
	@OneToMany(mappedBy="record", cascade = CascadeType.ALL)
	private List<TransferReport> transferReport;
	
	@OneToMany(mappedBy="record", cascade = CascadeType.ALL)
	private List<ClosingReport> closingReport;
}
