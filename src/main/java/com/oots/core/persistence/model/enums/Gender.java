package com.oots.core.persistence.model.enums;

public enum Gender {
	FEMALE("Femenino"),
	MALE("Masculino"),
	OTHER ("Otro");
	
	private String desc;
	Gender(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
