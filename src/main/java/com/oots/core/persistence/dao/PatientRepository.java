package com.oots.core.persistence.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oots.core.persistence.model.patient.FullName;
import com.oots.core.persistence.model.patient.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
	
	List<Patient> findByName(FullName name);
	
	List<Patient> findAll();
	
	Optional<Patient> findById(Long id);
}
