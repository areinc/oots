package com.oots.core.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oots.core.persistence.model.User;
import com.oots.core.persistence.model.record.Record;

@Repository
public interface RecordRepository extends JpaRepository<Record, Long> {
	List<Record> findByUser(User user);
}
