package com.oots.core.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oots.core.persistence.model.User;
import com.oots.core.persistence.model.record.Record;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    @Override
    void delete(User user);

    @Query(value="SELECT r FROM Record r where r.user.id = :id")
	List<Record> getUserRecords(@Param("id") Long id);
}
