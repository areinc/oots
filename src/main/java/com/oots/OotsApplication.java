package com.oots;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OotsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OotsApplication.class, args);
	}
}