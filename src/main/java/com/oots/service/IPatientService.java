package com.oots.service;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import com.oots.web.dto.PatientSearch;
import com.oots.web.dto.patient.PatientDTO;

public interface IPatientService {
	
	List<PatientDTO> search(PatientSearch search);
	
	HashMap<String, String> getAllPatientsNames();
	
	List<PatientDTO> getAllPatients() throws NullPointerException, ParseException;
	
	PatientDTO save(PatientDTO patient) throws NullPointerException, ParseException;
}
