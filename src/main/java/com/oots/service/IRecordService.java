package com.oots.service;

import java.text.ParseException;
import java.util.List;

import com.oots.web.dto.PatientSearch;
import com.oots.web.dto.record.RecordDTO;

public interface IRecordService {
	List<RecordDTO> search(PatientSearch search);
	List<RecordDTO> getAllRecords() throws NullPointerException, ParseException;
	RecordDTO save(RecordDTO record) throws NullPointerException, ParseException;
	RecordDTO findByID(Long id) throws NullPointerException, ParseException;
}
