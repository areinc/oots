package com.oots.service.implementation;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oots.core.persistence.dao.PatientRepository;
import com.oots.core.persistence.model.patient.Condition;
import com.oots.core.persistence.model.patient.ContactInformation;
import com.oots.core.persistence.model.patient.Education;
import com.oots.core.persistence.model.patient.EmergencyContact;
import com.oots.core.persistence.model.patient.Family;
import com.oots.core.persistence.model.patient.FamilyCondition;
import com.oots.core.persistence.model.patient.FamilyMember;
import com.oots.core.persistence.model.patient.Guardian;
import com.oots.core.persistence.model.patient.Hospitalization;
import com.oots.core.persistence.model.patient.Housing;
import com.oots.core.persistence.model.patient.Income;
import com.oots.core.persistence.model.patient.IncomeSource;
import com.oots.core.persistence.model.patient.Insurance;
import com.oots.core.persistence.model.patient.Job;
import com.oots.core.persistence.model.patient.Medicine;
import com.oots.core.persistence.model.patient.MentalHealth;
import com.oots.core.persistence.model.patient.Patient;
import com.oots.core.persistence.model.patient.PhysicalHealth;
import com.oots.core.persistence.model.record.Record;
import com.oots.service.IPatientService;
import com.oots.web.dto.PatientSearch;
import com.oots.web.dto.patient.AcademicHistoryDTO;
import com.oots.web.dto.patient.AddressDTO;
import com.oots.web.dto.patient.ConditionDTO;
import com.oots.web.dto.patient.ContactInformationDTO;
import com.oots.web.dto.patient.EducationDTO;
import com.oots.web.dto.patient.EmergencyContactDTO;
import com.oots.web.dto.patient.FamilyConditionDTO;
import com.oots.web.dto.patient.FamilyDTO;
import com.oots.web.dto.patient.FamilyMemberDTO;
import com.oots.web.dto.patient.FullNameDTO;
import com.oots.web.dto.patient.GuardianDTO;
import com.oots.web.dto.patient.HospitalizationDTO;
import com.oots.web.dto.patient.HousingDTO;
import com.oots.web.dto.patient.IncomeDTO;
import com.oots.web.dto.patient.InsuranceDTO;
import com.oots.web.dto.patient.JobDTO;
import com.oots.web.dto.patient.MedicineDTO;
import com.oots.web.dto.patient.MentalHealthDTO;
import com.oots.web.dto.patient.PatientDTO;
import com.oots.web.dto.patient.PhysicalHealthDTO;
import com.oots.web.dto.patient.PsychiatricTreatmentDTO;
import com.oots.web.dto.record.RecordDTO;
import com.oots.web.util.DateConverter;

@Service
@Transactional
public class PatientService implements IPatientService{
	@Autowired
	private PatientRepository patientRepository;
	
	@Autowired
	private RecordService recordService;
	
	protected PatientDTO convertModelToDto(Patient p, boolean withRecord) throws NullPointerException, ParseException {
		PatientDTO patientDto = new PatientDTO();
		
		// Personal Info
		patientDto.setId(p.getId());
		patientDto.setName(new FullNameDTO(p.getName().getFirstName(), p.getName().getMiddleName(), p.getName().getLastName(), p.getName().getMaidenName(), p.getName().getNickname()));
		patientDto.setDateOfBirth(DateConverter.stringFromDate(p.getDateOfBirth()));
		patientDto.setGender(p.getGender());
		patientDto.setCivilState(p.getCivilState());
		patientDto.setReligion(p.getReligion());
		
		// Contact Info
		AddressDTO ciResidential = new AddressDTO(p.getContactInfo().getResidentialAddress().getFirstLine(), p.getContactInfo().getResidentialAddress().getSecondLine(), p.getContactInfo().getResidentialAddress().getThirdLine(), p.getContactInfo().getResidentialAddress().getCity(), p.getContactInfo().getResidentialAddress().getState(), p.getContactInfo().getResidentialAddress().getCountry(), p.getContactInfo().getResidentialAddress().getPostalCode());
		AddressDTO ciPostal = new AddressDTO(p.getContactInfo().getPostalAddress().getFirstLine(), p.getContactInfo().getPostalAddress().getSecondLine(), p.getContactInfo().getPostalAddress().getThirdLine(), p.getContactInfo().getPostalAddress().getCity(), p.getContactInfo().getPostalAddress().getState(), p.getContactInfo().getPostalAddress().getCountry(), p.getContactInfo().getPostalAddress().getPostalCode());
		ContactInformationDTO contactInfo = new ContactInformationDTO(ciResidential, ciPostal, p.getContactInfo().getHomePhone(), p.getContactInfo().getMobilePhone(), p.getContactInfo().getWorkPhone(), p.getContactInfo().getEmail());
		patientDto.setContactInfo(contactInfo);
		
		// Education
		List<EducationDTO> education = new ArrayList<EducationDTO>();
		for(Education e : p.getAcademicHistory().getEducation()) {
			education.add(new EducationDTO(e.getScholarity(), e.getConcentration(), e.getSchool()));
		}
		patientDto.setAcademicHistory(new AcademicHistoryDTO(p.getAcademicHistory().getHistory(), education));
		
		// Job
		patientDto.setJob(new JobDTO(p.getJob().getCompany(), p.getJob().getPosition(), p.getJob().getTime(), p.getJob().isHadDifficulties(), p.getJob().getDifficulties()));
		
		// Income
		IncomeDTO income = new IncomeDTO();
		income.setMontlyIncomeEstimate(p.getIncome().getMontlyIncomeEstimate());
		income.setMontlyIncome(p.getIncome().getMontlyIncome());
		
		for(IncomeSource is : p.getIncome().getSources()) {
			switch (is.getSource()) {
			case SALARY:
				income.setSalary(is.getIncome());
				break;
			case SOCIAL_SECURITY:
				income.setSocialSecurity(is.getIncome());
				break;
			case PENSION:
				income.setPension(is.getIncome());
				break;
			case EBT:
				income.setEbt(is.getIncome());
				break;
			case CHILD_SUPPORT:
				income.setChildSupport(is.getIncome());
				break;
			case RENT:
				income.setRent(is.getIncome());
				break;
			case UNEMPLOYMENT:
				income.setUnemployment(is.getIncome());
				break;
			case OWN_BUSINESS:
				income.setBusiness(is.getIncome());
				break;
			case OTHER:
				income.setOther(is.getIncome());
				break;
			}
		}
		patientDto.setIncome(income);
		
		// Insurance
		patientDto.setInsurance(new InsuranceDTO(p.getInsurance().getName(), p.getInsurance().getMedicare(), p.getInsurance().getComplementary(), DateConverter.stringFromDate(p.getInsurance().getExpDate())));
		
		// Housing
		patientDto.setHousing(new HousingDTO(p.getHousing().getDescription(), p.getHousing().getType(), p.getHousing().getOtherType(), p.getHousing().getMaterials(), p.getHousing().getOtherMaterials(), p.getHousing().getBedrooms(), p.getHousing().getBathrooms(), p.getHousing().getComponents(), p.getHousing().getOtherComponents(), p.getHousing().getOwnership(), p.getHousing().getOtherOwnership()));
		
		// Guardian
		FullNameDTO guardianName = new FullNameDTO(p.getGuardian().getName().getFirstName(), p.getGuardian().getName().getMiddleName(), p.getGuardian().getName().getLastName(), p.getGuardian().getName().getMaidenName(), p.getGuardian().getName().getNickname());
		AddressDTO guardianResidential = new AddressDTO(p.getGuardian().getContactInfo().getResidentialAddress().getFirstLine(), p.getGuardian().getContactInfo().getResidentialAddress().getSecondLine(), p.getGuardian().getContactInfo().getResidentialAddress().getThirdLine(), p.getGuardian().getContactInfo().getResidentialAddress().getCity(), p.getGuardian().getContactInfo().getResidentialAddress().getState(), p.getGuardian().getContactInfo().getResidentialAddress().getCountry(), p.getGuardian().getContactInfo().getResidentialAddress().getPostalCode());
		AddressDTO guardianPostal = new AddressDTO(p.getGuardian().getContactInfo().getPostalAddress().getFirstLine(), p.getGuardian().getContactInfo().getPostalAddress().getSecondLine(), p.getGuardian().getContactInfo().getPostalAddress().getThirdLine(), p.getGuardian().getContactInfo().getPostalAddress().getCity(), p.getGuardian().getContactInfo().getPostalAddress().getState(), p.getGuardian().getContactInfo().getPostalAddress().getCountry(), p.getGuardian().getContactInfo().getPostalAddress().getPostalCode());
		ContactInformationDTO guardianContactInfo = new ContactInformationDTO(guardianResidential, guardianPostal, p.getGuardian().getContactInfo().getHomePhone(), p.getGuardian().getContactInfo().getMobilePhone(), p.getGuardian().getContactInfo().getWorkPhone(), p.getGuardian().getContactInfo().getEmail());
		patientDto.setGuardian(new GuardianDTO(guardianName, guardianContactInfo));
		
		// Emergency Contact
		FullNameDTO emergencyName = new FullNameDTO(p.getEmergencyContact().getName().getFirstName(), p.getEmergencyContact().getName().getMiddleName(), p.getEmergencyContact().getName().getLastName(), p.getEmergencyContact().getName().getMaidenName(), p.getEmergencyContact().getName().getNickname());
		AddressDTO emergencyResidential = new AddressDTO(p.getEmergencyContact().getContactInfo().getResidentialAddress().getFirstLine(), p.getEmergencyContact().getContactInfo().getResidentialAddress().getSecondLine(), p.getEmergencyContact().getContactInfo().getResidentialAddress().getThirdLine(), p.getEmergencyContact().getContactInfo().getResidentialAddress().getCity(), p.getEmergencyContact().getContactInfo().getResidentialAddress().getState(), p.getEmergencyContact().getContactInfo().getResidentialAddress().getCountry(), p.getEmergencyContact().getContactInfo().getResidentialAddress().getPostalCode());
		AddressDTO emergencyPostal = new AddressDTO(p.getEmergencyContact().getContactInfo().getPostalAddress().getFirstLine(), p.getEmergencyContact().getContactInfo().getPostalAddress().getSecondLine(), p.getEmergencyContact().getContactInfo().getPostalAddress().getThirdLine(), p.getEmergencyContact().getContactInfo().getPostalAddress().getCity(), p.getEmergencyContact().getContactInfo().getPostalAddress().getState(), p.getEmergencyContact().getContactInfo().getPostalAddress().getCountry(), p.getEmergencyContact().getContactInfo().getPostalAddress().getPostalCode());
		ContactInformationDTO emergencyContactInfo = new ContactInformationDTO(emergencyResidential, emergencyPostal, p.getEmergencyContact().getContactInfo().getHomePhone(), p.getEmergencyContact().getContactInfo().getMobilePhone(), p.getEmergencyContact().getContactInfo().getWorkPhone(), p.getEmergencyContact().getContactInfo().getEmail());
		patientDto.setEmergencyContact(new EmergencyContactDTO(emergencyName, emergencyContactInfo, p.getEmergencyContact().getRelationship()));
		
		// Family
		List<FamilyMemberDTO> family = new ArrayList<FamilyMemberDTO>();
		for (FamilyMember fm : p.getFamily().getFamilyMembers()) {
			family.add(new FamilyMemberDTO(fm.getName(), fm.getAge(), fm.getRelationship(), fm.getScholarity(), fm.getOccupation()));
		}
		patientDto.setFamily(new FamilyDTO(p.getFamily().getHistory(), family));
		
		// Physical Health
		List<ConditionDTO> phConditions = new ArrayList<ConditionDTO>();
		for(Condition c : p.getPhysicalHealth().getConditions()) {
			List<MedicineDTO> medicine = new ArrayList<MedicineDTO>();
			
			for(Medicine m : c.getMedicine()) {
				medicine.add(new MedicineDTO(m.getName(), m.getAmount(), m.getDose()));
			}
			
			phConditions.add(new ConditionDTO(c.getName(), c.getTreatment(), medicine));
		}
		
		List<FamilyConditionDTO> phFamilyHistory = new ArrayList<FamilyConditionDTO>();
		for(FamilyCondition fc : p.getPhysicalHealth().getFamilyHistory()) {
			phFamilyHistory.add(new FamilyConditionDTO(fc.getCondition(), fc.getFamilySide()));
		}
		
		List<HospitalizationDTO> phHospitalizations = new ArrayList<HospitalizationDTO>();
		for(Hospitalization h : p.getPhysicalHealth().getHospitalizations()) {
			phHospitalizations.add(new HospitalizationDTO(DateConverter.stringFromDate(h.getDate()), h.getInstitution(), h.getType(), h.getReason()));
		}
		
		List<HospitalizationDTO> operations = new ArrayList<HospitalizationDTO>();
		for(Hospitalization o : p.getPhysicalHealth().getOperations()) {
			operations.add(new HospitalizationDTO(DateConverter.stringFromDate(o.getDate()), o.getInstitution(), o.getType(), o.getReason()));
		}
		
		patientDto.setPhysicalHealth(new PhysicalHealthDTO(p.getPhysicalHealth().getHistory(), phConditions, phFamilyHistory, p.getPhysicalHealth().isHospitalized(), p.getPhysicalHealth().getAmountOfHospitalizations(), phHospitalizations, p.getPhysicalHealth().isOperated(), p.getPhysicalHealth().getAmountOfOperations(), operations));
		
		// Mental Health
		List<ConditionDTO> mhConditions = new ArrayList<ConditionDTO>();
		for(Condition c : p.getMentalHealth().getConditions()) {
			List<MedicineDTO> medicine = new ArrayList<MedicineDTO>();
			
			for(Medicine m : c.getMedicine()) {
				medicine.add(new MedicineDTO(m.getName(), m.getAmount(), m.getDose()));
			}
			
			mhConditions.add(new ConditionDTO(c.getName(), c.getTreatment(), medicine));
		}
		
		List<FamilyConditionDTO> mhFamilyHistory = new ArrayList<FamilyConditionDTO>();
		for(FamilyCondition fc : p.getMentalHealth().getFamilyHistory()) {
			mhFamilyHistory.add(new FamilyConditionDTO(fc.getCondition(), fc.getFamilySide()));
		}
		
		List<HospitalizationDTO> mhHospitalizations = new ArrayList<HospitalizationDTO>();
		for(Hospitalization h : p.getMentalHealth().getHospitalizations()) {
			mhHospitalizations.add(new HospitalizationDTO(DateConverter.stringFromDate(h.getDate()), h.getInstitution(), h.getType(), h.getReason()));
		}
		
		PsychiatricTreatmentDTO psychiatricTreatment = new PsychiatricTreatmentDTO(p.getMentalHealth().getPsychiatricTreatment().getDoctor(), p.getMentalHealth().getPsychiatricTreatment().getSpecialty(), p.getMentalHealth().getPsychiatricTreatment().getPhone(), p.getMentalHealth().getPsychiatricTreatment().getAddress());
		
		patientDto.setMentalHealth(new MentalHealthDTO(p.getMentalHealth().getHistory(), mhConditions, psychiatricTreatment, mhFamilyHistory, p.getPhysicalHealth().isHospitalized(), p.getPhysicalHealth().getAmountOfHospitalizations(), mhHospitalizations));
		
		// Record
		if(withRecord) {
			List<RecordDTO> records = new ArrayList<RecordDTO>();
			for(Record r : p.getRecords()) {
				records.add(recordService.convertModelToDto(r, false));
			}
			patientDto.setRecords(records);
		}
		
		return patientDto;
	}
	
	public Patient getPatientByID(Long id){
		Optional<Patient> patient = patientRepository.findById(id);
		
		if (patient.isPresent()) {
			return patient.get();
		}
		
		return null;
	}
	
	public PatientDTO getPatientDTOByID(Long id) throws NullPointerException, ParseException {
		Patient patient = getPatientByID(id);
		
		if (patient != null) {
			return convertModelToDto(patient, true);
		}
		
		return null;
	}
	
	@Override
	@Transactional
	public List<PatientDTO> search(PatientSearch search) {
		return null;
	}
	
	@Override
	public HashMap<String, String> getAllPatientsNames() {
		HashMap<String, String> patients = new HashMap<>();

		List<Patient> list = patientRepository.findAll();
		
		for (Patient p : list) {
			String name = new FullNameDTO(p.getName().getFirstName(), p.getName().getMiddleName(), p.getName().getLastName(), p.getName().getMaidenName(), "").getFullName();
			String id = "" + p.getId();
			
			patients.put(id, name);
		}
		
		return patients;
	}
	
	public List<PatientDTO> getAllPatientsDTO() {
		List<PatientDTO> patients = new ArrayList<PatientDTO>();
		List<Patient> list = patientRepository.findAll();
		
		for (Patient p : list) {
			try {
				patients.add(convertModelToDto(p, false));
			} catch (NullPointerException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return patients;
	}
	
	@Override
	public List<PatientDTO> getAllPatients() throws NullPointerException, ParseException {
		List<PatientDTO> patienList = new ArrayList<PatientDTO>();
		
		for (Patient u : patientRepository.findAll()) {
			patienList.add(convertModelToDto(u, true));
		}
		
		return patienList;
	}
	
	@Override
	public PatientDTO save(PatientDTO patient) throws NullPointerException, ParseException {
		Patient p = new Patient();
		
		// Name
//    	FullName name = new FullName(patient.getPersonalInformation().getName().getFirstName(), patient.getPersonalInformation().getName().getMiddleName(), patient.getPersonalInformation().getName().getLastName(), patient.getPersonalInformation().getName().getMaidenName(), patient.getPersonalInformation().getName().getNickname());
//    	p.setName(name);
    	
    	//DoB
    	if (!StringUtils.isEmpty(patient.getDateOfBirth())) {
	    	try {
				DateFormat format = new SimpleDateFormat("d 'de' MMMM 'del' yyyy",  Locale.forLanguageTag("es-PR"));
				DateTime dob = new DateTime(format.parse(patient.getDateOfBirth()));
				
				p.setDateOfBirth(dob.toDate());
			} catch (ParseException e) {
				e.printStackTrace();
			}
    	}
    	
    	// Guardian
    	Guardian guardian = new Guardian();
    	p.setGuardian(guardian);
    	
    	// Contact Info
    	ContactInformation contactInfo = new ContactInformation();
    	p.setContactInfo(contactInfo);
    	
    	// Housing
    	Housing housing = new Housing();
    	p.setHousing(housing);
    	
    	// Job
    	Job job = new Job();
    	p.setJob(job);
    	
    	// Income
    	Income income = new Income();
    	p.setIncome(income);
    	
    	// Emergency Contact
    	EmergencyContact emergencyContact = new EmergencyContact();
    	p.setEmergencyContact(emergencyContact);
    	
    	// Family
    	Family family = new Family();
    	p.setFamily(family);
    	
    	// Physical Health
    	MentalHealth mentalHealth = new MentalHealth();
    	p.setMentalHealth(mentalHealth);
    	
    	// Mental Health
    	PhysicalHealth physicalHealth = new PhysicalHealth();
    	p.setPhysicalHealth(physicalHealth);
    	
    	// Insurance
    	Insurance insurance = new Insurance();
    	p.setInsurance(insurance);
    	
/**
    	// Guardian
    	FullName guardianName = new FullName("Maria", "Caridad", "Otero", "Arroyo", "null");
    	Address guardianAddress = new Address("calle 41 SE", null, null, "San Juan", null, "Puerto Rico", "00921");
    	ContactInformation guardianContact = new ContactInformation(guardianAddress, guardianAddress, "7873464875", null, null, null);
    	Guardian guardian = new Guardian(guardianName, guardianContact);
    	
    	// Contact Info
    	Address postal = new Address("200 Ave Los Chalets Apt 36", "Cond Chalets de Cupey", null, "San Juan", null, "Puerto Rico", "00926");
    	Address residential = new Address("200 Ave Los Chalets Apt E-409", "Cond Chalets de Cupey", null, "San Juan", null, "Puerto Rico", "00926");
    	ContactInformation contactInfo = new ContactInformation(residential, postal, null, "7873464257", "7877599999", "eliezer.ferra@gmail.com");
    	
    	// Housing
    	Housing housing = new Housing("This is a description", HousingType.APARTMENT, null, HousingMaterial.CONCRETE, null, "3", "2", Arrays.asList(HousingComponents.DINING_ROOM, HousingComponents.LAUNDRY_ROOM), HousingOwnership.RENT, null);
    	
    	// Job
    	Job job = new Job("Evertec", "Product Manager", "2 aÃ±os", false, null);
    	
    	// Income
    	IncomeSource source1 = new IncomeSource(IncomeSources.SALARY, 50000);
    	IncomeSource source2 = new IncomeSource(IncomeSources.RENT, 5000);
    	Income income = new Income(IncomeEstimated.MORE_5000, 55000, Arrays.asList(source1, source2));
    	
    	// Emergency Contact
    	FullName emergencyName = new FullName("Iris", "Yanice", "Rivera", "Torres", null);
    	Address emergencyPostal = new Address("200 Ave Los Chalets Apt 36", "Cond Chalets de Cupey", null, "San Juan", null, "Puerto Rico", "00926");
    	ContactInformation emergencyContactInfo = new ContactInformation(emergencyPostal, emergencyPostal, null, "7872353428", null, "i.yaniice@gmail.com");
    	EmergencyContact emergencyContact = new EmergencyContact(emergencyName, emergencyContactInfo, "wife");
    	
    	// Family
    	FamilyMember member1 = new FamilyMember("Iris Yanice Rivera", 27, "wife", Scholarity.ASSOCIATES, null);
    	FamilyMember member2 = new FamilyMember("Nephtali Ferra Santiago", 16, "nephew", Scholarity.HIGH_SCHOOL, null);
    	Family family = new Family("This is a small history", Arrays.asList(member1, member2));
    	
    	// Physical Health
    	Medicine medicine = new Medicine("Fliorizet", "2 tabletas", "cada 4 horas");
    	Condition condition = new Condition("MigraÃ±a", Treatment.YES, Arrays.asList(medicine));
    	FamilyCondition familyCondition = new FamilyCondition("Diabetes", FamilySide.MATERNAL);
    	Hospitalization hosp = new Hospitalization(new Date(), "Institucion", HospitalizationType.NA, "idk");
    	MentalHealth mentalHealth = new MentalHealth("History", Arrays.asList(condition), Arrays.asList(familyCondition), true, 1, Arrays.asList(hosp));
    	
    	// Mental Health
    	Medicine medicine1 = new Medicine("Zanax", "2 tabletas", "cada 4 horas");
    	Condition condition1 = new Condition("Loquera", Treatment.YES, Arrays.asList(medicine1));
    	FamilyCondition familyCondition1 = new FamilyCondition("Depresion", FamilySide.MATERNAL);
    	PhysicalHealth physicalHealth = new PhysicalHealth("History", Arrays.asList(condition1), Arrays.asList(familyCondition1), false, 0, null, false, 0, null);
    	
    	// Insurance
    	Insurance insurance = new Insurance("SSS", Medicare.NOT_APPLY, null, new Date());
**/
    	Patient newPatient = patientRepository.save(p);
		
		return convertModelToDto(newPatient, true);
	}
}
