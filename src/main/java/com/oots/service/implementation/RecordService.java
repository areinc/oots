package com.oots.service.implementation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oots.core.persistence.dao.RecordRepository;
import com.oots.core.persistence.model.record.Record;
import com.oots.service.IRecordService;
import com.oots.web.dto.PatientSearch;
import com.oots.web.dto.record.RecordDTO;

@Service
@Transactional
public class RecordService implements IRecordService{
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PatientService patientService;
	
	@Autowired
	private RecordRepository recordRepository;
	
	protected RecordDTO convertModelToDto(Record r, boolean withPatient) throws NullPointerException, ParseException {
		RecordDTO record = new RecordDTO();
		
		record.setId(r.getId());
		record.setStatus(r.getStatus());
		record.setUser(userService.convertModelToDto(r.getUser()));
		
		if (withPatient) {
			record.setPatient(patientService.convertModelToDto(r.getPatient(), false));
		}
		
//		List<DocumentDTO> documents = new ArrayList<DocumentDTO>();
//		for(Document doc : r.getDocuments()) {
//			documents.add(convertModelToDto(doc));
//		}
//		record.setDocuments(documents);
		
		return record;
	}
	
//	private DocumentDTO convertModelToDto(Document d) {
//		DocumentDTO document = new DocumentDTO();
//		
//		document.setId(d.getId());
//		document.setFileType(d.getFileType());
//		document.setDocumentType(d.getDocumentType());
//		document.setFile(d.getFile());
//		
//		return null;
//	}
	
	@Override
	public List<RecordDTO> search(PatientSearch search) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RecordDTO> getAllRecords() throws NullPointerException, ParseException {
		List<Record> records = recordRepository.findAll();
		List<RecordDTO> recordsDTO = new ArrayList<RecordDTO>();
		
		for(Record r : records) {
			recordsDTO.add(convertModelToDto(r, true));
		}
		
		return recordsDTO;
	}

	@Override
	public RecordDTO save(RecordDTO tmpRecord) throws NullPointerException, ParseException {
		Record record = new Record();
		
		return convertModelToDto(record, true);
	}

	@Override
	public RecordDTO findByID(Long id) throws NullPointerException, ParseException {
		Optional<Record> optRecord = recordRepository.findById(id);
		
		if(!optRecord.isPresent()) {
			return null;
		}
		
		Record record = optRecord.get();
		
		return convertModelToDto(record, true);
	} 
	
}
