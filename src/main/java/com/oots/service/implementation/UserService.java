package com.oots.service.implementation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.oots.core.persistence.dao.RoleRepository;
import com.oots.core.persistence.dao.UserRepository;
import com.oots.core.persistence.model.User;
import com.oots.core.persistence.model.enums.UserStatus;
import com.oots.core.persistence.model.patient.FullName;
import com.oots.core.persistence.model.record.Record;
import com.oots.service.IUserService;
import com.oots.web.dto.UserDTO;
import com.oots.web.dto.patient.FullNameDTO;
import com.oots.web.error.UserAlreadyExistException;

@Service
@Transactional
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private SessionRegistry sessionRegistry;
    
    protected UserDTO convertModelToDto(User u) {
    	UserDTO user = new UserDTO();
    	
    	user.setId(u.getId());
    	user.setEmail(u.getEmail());
    	user.setName(new FullNameDTO(u.getName().getFirstName(), u.getName().getMiddleName(), u.getName().getLastName(), u.getName().getMaidenName(), u.getName().getNickname()));
    	
    	return user;
    	
    }

    // API
    public HashMap<String, String> getAllActiveUsersNames() {
		HashMap<String, String> users = new HashMap<>();
		
		for (User u : userRepository.findAll()) {
			if (u.getStatus() != UserStatus.INACTIVE) {
				String name = new FullNameDTO(u.getName().getFirstName(), u.getName().getMiddleName(), u.getName().getLastName(), u.getName().getMaidenName(), "").getFullName();
				String id = u.getId().toString();
				
				users.put(id, name);
			}
		}
		
		return users;
	}

    @Override
    public User registerNewUserAccount(final UserDTO accountDto) {
        if (emailExist(accountDto.getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email adress: " + accountDto.getEmail());
        }
        final User user = new User();
        
        FullName name = new FullName(accountDto.getName().getFirstName(), null, accountDto.getName().getLastName(), null, null);

        user.setName(name);
        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        user.setEmail(accountDto.getEmail());
        user.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));
        user.setStatus(UserStatus.ACTIVE);
        
        return userRepository.save(user);
    }

    @Override
    public void saveRegisteredUser(final User user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUser(final User user) {
        userRepository.delete(user);
    }

    @Override
    public User findUserByEmail(final String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User getUserByID(final long id) {
    	Optional<User> user = userRepository.findById(id);
    	
    	if(user.isPresent()) {
    		return user.get();
    	}
    	
        return null;
    }

    @Override
    public void changeUserPassword(final User user, final String password) {
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    @Override
    public boolean checkIfValidOldPassword(final User user, final String oldPassword) {
        return passwordEncoder.matches(oldPassword, user.getPassword());
    }

    @Override
    public User updateUser2FA(boolean use2FA) {
        final Authentication curAuth = SecurityContextHolder.getContext()
            .getAuthentication();
        User currentUser = (User) curAuth.getPrincipal();
        currentUser = userRepository.save(currentUser);
        final Authentication auth = new UsernamePasswordAuthenticationToken(currentUser, currentUser.getPassword(), curAuth.getAuthorities());
        SecurityContextHolder.getContext()
            .setAuthentication(auth);
        return currentUser;
    }

    private boolean emailExist(final String email) {
        return userRepository.findByEmail(email) != null;
    }

    @Override
    public List<String> getUsersFromSessionRegistry() {
        return sessionRegistry.getAllPrincipals()
            .stream()
            .filter((u) -> !sessionRegistry.getAllSessions(u, false)
                .isEmpty())
            .map(o -> {
                if (o instanceof User) {
                    return ((User) o).getEmail();
                } else {
                    return o.toString();
                }
            })
            .collect(Collectors.toList());
    }
    
    @Override
    public List<Record> getUserRecords(Long userId) {
    	return userRepository.getUserRecords(userId);
    }
}
