package com.oots.service;

import java.util.List;

import com.oots.core.persistence.model.User;
import com.oots.core.persistence.model.record.Record;
import com.oots.web.dto.UserDTO;
import com.oots.web.error.UserAlreadyExistException;

public interface IUserService {

    User registerNewUserAccount(UserDTO accountDto) throws UserAlreadyExistException;

    void saveRegisteredUser(User user);

    void deleteUser(User user);

    User findUserByEmail(String email);

    User getUserByID(long id);

    void changeUserPassword(User user, String password);

    boolean checkIfValidOldPassword(User user, String password);

    User updateUser2FA(boolean use2FA);

    List<String> getUsersFromSessionRegistry();
    
    List<Record> getUserRecords(Long userId);
}
